# Announcement

This is a small project to elaborate and maintain the announcement of ROOTY. 

The announcement itself is a mark down file: https://gitlab.com/c4319/announcement/-/blob/main/announcement.md

It is a project because the announcement is surrounded by issues and comments and it must be managed and promoted.

Embedding the project in gitlab demonstrates the involvement of separate teams and illustrates progress, recent changes and history.
